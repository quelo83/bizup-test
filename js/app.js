var tabs = (function() {

    var init = function() {
        bindEvents();
    };

    var bindEvents = function() {
        $('.nav-tabs a').on('click', function(e) {
            e.preventDefault();

            var id = $(this).attr('href').replace('#', '');

            // Remove active class
            $(this).closest('ul').find('li').removeClass('active');
            $('.tab-content .tab-pane').removeClass('active');

            // Add active class
            $(this).parent().addClass('active');
            $('.tab-content .tab-pane#' + id).addClass('active');
        });

    };

    // Public API
    return {
        init: init
    };

})();



var accordion = (function() {

    var init = function() {
        bindEvents();
    };

    var bindEvents = function() {
        $('.accordion .header').on('click', function(e) {
            e.preventDefault();

            $(this).parent().toggleClass('opened');
        });

    };

    // Public API
    return {
        init: init
    };

})();



var app = (function() {

    var init = function() {
        tabs.init();
        accordion.init();
        console.log('App initialized!');
    };

    // Public API
    return {
        init: init
    };

})();



// Run application!
$(document).ready(function() {
    app.init();
});